# -*- coding: utf-8 -*-


from openerp import models, fields, api, _
from openerp.exceptions import Warning, UserError, AccessError
from datetime import datetime, date
from openerp.tools.translate import _



class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    # def _compute_qty(self):
    #     for data in self:
    #         sum_total= 0.0
    #         for line in data.order_line:
    #             sum_total= sum_total + line.product_qty
    #             sum_total= sum_total * line.price_unit
    #         data.total= sum_total

    # @api.onchange('diskon')
    # def get_price(self):
    #     self.afterDis= self.total - (self.total * self.diskon/100)

    #   TASK 1
    reference = fields.Char(string="Customer Reference")
    #total = fields.Integer(string="Total Price")
    # total = fields.Integer(compute='_compute_qty')
    phone = fields.Char(related='partner_id.phone')
    emil = fields.Char(related='partner_id.email')
    # diskon= fields.Float(string="Diskon")
    # afterDis= fields.Float(string="Total_diskon")

    #   TASK 2
    user= fields.Many2one("res.users", default = lambda self: self.env.user, string="Created")

    #   TASK 3
    # expec_date= fields.string(related='order_line.date_planned')
    duration = fields.Float(string="Duration",compute='_compute_duration')

    @api.one
    @api.depends('date_planned','date_order')
    def _compute_duration(self):
        if not self.date_planned:
            self.date_planned = fields.Date.today()

        date_come = fields.Date.from_string(self.date_planned)
        date_request = fields.Date.from_string(self.date_order)
        calc = date_come - date_request
        self.duration = calc.days

    #   TASK 4
    @api.multi
    def action_view_related_products(self):
        list_product = [line.product_id.id for line in self.order_line]

        return{
            'name':'product',
            'view_mode': 'tree',
            'res_model': 'product.product',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain':[('id','in',list_product)]}
    
    #   TASK 6
    @api.model
    def create(self,vals):
        res = super(PurchaseOrder,self).create(vals)
        if not self.partner_ref:
            res['partner_ref'] = 'No Vendor Reference'
        return res

    @api.multi
    def write(self,vals):
        res = super(PurchaseOrder,self).write(vals)
        if not self.partner_ref:
            self.partner_ref = 'No Vendor Reference'
        return res

    #     @api.model
    # def create(self, vals):
    #     if vals.get('name', 'New') == 'New':
    #         vals['name'] = self.env['ir.sequence'].next_by_code('purchase.order') or '/'
    #     return super(PurchaseOrder, self).create(vals)
  
    
    #   TASK 9
    down_payment= fields.Float(string='Down Payment (DP)')
    amount_total_dp = fields.Float(string='Total', compute='_compute_DP')
    

    @api.depends('amount_total')
    def _compute_DP(self):
        self.amount_total_dp = self.amount_total - self.down_payment
        # l_amount_total = self.amount_total
        # l_down_payment = self.down_payment
        # sums= l_amount_total - l_down_payment
        # self.amount_total_dp = sums

        

        
    # @api.onchange('diskon')
    # def get_price(self):
    #     self.afterDis= self.total - (self.total * self.diskon/100)
    # @api.depends('order_line.price_total')
    # def _amount_all(self):
    #     for order in self:
    #         amount_untaxed = amount_tax = 0.0
    #         for line in order.order_line:
    #             amount_untaxed += line.price_subtotal
    #             amount_tax += line.price_tax
    #         order.update({
    #             'amount_untaxed': order.currency_id.round(amount_untaxed),
    #             'amount_tax': order.currency_id.round(amount_tax),
    #             'amount_total': amount_untaxed + amount_tax,
    #         })


    #   TASK 11
class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def action_cancel(self):
        search_document_obj = self.env['purchase.order']
        search_origin = self.name
        search_document = search_document_obj.search([('origin', '=', search_origin)])
        print search_document, 'RRRRRRRRRRRRRRRRRRRRRRRR'
        for document in search_document:
            print 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
            document.write({'state': 'cancel'})
        return super(SaleOrder, self).action_cancel()

    @api.multi
    def action_draft(self):
        search_document_obj = self.env['purchase.order']
        search_origin = self.name
        search_document = search_document_obj.search([('origin', '=', search_origin)])
        print search_document, 'RRRRRRRRRRRRRRRRRRRRRRRR'
        for document in search_document:
            print 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
            document.write({'state': 'draft'})
        return super(SaleOrder, self).action_cancel()
        
        # self.write({'state': 'cancel'})


    #   TASK 5
class StockPicking(models.Model):
    _inherit = 'stock.picking'

    # confirm_date =fields.Datetime(string='Confirm Date')
    confirm_date=fields.Datetime(string="Confirmed Date", readonly=True)
    confirm_user=fields.Many2one('res.users',string ='Confirmed By', readonly=True)
    # confirm_user= fields.Many2one("res.users", default = lambda self: self.env.user, string="Confirmed By")

    @api.multi
    def action_confirm(self):
        self.confirm_user= self.env.user
        self.confirm_date= datetime.now()
        return super(StockPicking, self).action_confirm()





class ProductTemplate(models.Model):
    _inherit = 'product.template'

    # TASK 10

    @api.model
    def create(self, vals):
        self= self.with_context(from_create=True)
        res= super(ProductTemplate, self).create(vals)
        if not vals['seller_ids']:
            raise Warning('You should fill in the supplier details, at least one')
        return res
        
     # vals_supplier =vals['seller_ids']
     # print vals_supplier , "++++++++++++++++="
     # self = self.with_context(dari_create=True) #untuk membuat wadah baru agar tidak mengambil funtion write
        # res = super(ProductTemplate, self).create(vals)

    @api.multi
    def write(self, vals):
        seller_ada= False
        if self.env.context.get('from_create') == True:
            seller_ada = True
        elif seller_ada == False:
            res= super(ProductTemplate, self).write(vals)
            if res:
                for data in self:
                    if not data.seller_ids:
                        raise Warning('You should fill in the supplier details, at least one')
            return res

        # if not self.seller_ids:
        #     # raise osv.except_osv(_("Warning!"), _(" Hello Mehdi Mokni !!."))
        #     raise UserError(_('You Should fill in supplier details, at least one'))
        # return super(ProductTemplate, self).write()





    #     return res

    #   TASK 7
    # @api.multi
    # def write(self, vals):
    #     res = super(ProductTemplate, self).write(vals)
    #     if not self.seller_ids:
    #         supplier_obj = self.env['product.supplierinfo']
    #         res_partner_obj = self.env['res.partner'].search([('name','=','no supplier')])
    #         cek = supplier_obj.create({
    #             'product_tmpl_id' : self.id,
    #             'name' :res_partner_obj.id,
    #             'delay':1,
    #             'min_qty':0,
    #             'price':0})
    #             #     print "000000000000000000"
    #             # print "===================1234"
    #     return res 
     
    # @api.model
    # def create(self, vals):
    #  # vals_supplier =vals['seller_ids']
    #  # print vals_supplier , "++++++++++++++++="
    #  # self = self.with_context(dari_create=True) #untuk membuat wadah baru agar tidak mengambil funtion write
    #     res = super(ProductTemplate, self).create(vals)

    #     return res

#   TASK 7 eror edit
#     @api.model
#     def create(self, vals):
#         vals_supplier =vals['seller_ids']
#         print vals_supplier , "++++++++++++++++="
#         self = self.with_context(dari_create=True) #untuk membuat wadah baru agar tidak mengambil funtion write
#         res = super(ProductTemplate, self).create(vals)
#         if not vals_supplier: #kalo kosong
#             res_partner_obj = self.env['res.partner'] #akses res.partner, search name no supplier, akses suppinfo,
#             res_partner = res_partner_obj.search([('name', '=', 'no supplier')])
#             prod_supplier = self.env['product.supplierinfo']
             
#             if res_partner: #kalo ada no supplier, di create res partnernya.
#                 prod_supplier.create({
#                                       'product_tmpl_id' : res.id,
#                                       'name' : res_partner.id,
#                                       'delay' : 1,
#                                       'min_qty' : 3,
#                                       'price' : 200,
                                      
#                                       })     
#             else:
#                 vendor = res_partner_obj.create({
#                                         'name' : "no supplier",
#                                         'supplier' : True #coba
#                                                 })
#                 prod_supplier.create({
#                                   'product_tmpl_id' : res.id,
#                                   'name' : vendor.id,
#                                   'delay' : 1,
#                                   'min_qty' : 3,
#                                   'price' : 200,
#                                     })
#         return res
    
    
#     #    masih eror. kalo ga ada supplier namanya no supplier, waktu di edit ngga keluar. tapi kalo sudah create no supplier, dia keluar.
#     @api.multi
#     def write(self, vals):
#         ada_seller = False #lanjutan dari create
#         if self.env.context.get("dari_create") == True : #lanjutan dari create agar tidak keluar warning
#             print "=============2"  ##kalo dari_create nya true maka ada sellernya true.
#             ada_seller = True           ## else kalo ada_seller nya false, maka:
#             print "================3"       ## harusnya kita cek res.usernya, kalo ada maka langsung tambah. kalo ngga ada maka bikin dlu resusernya
#         seller = self.env['product.supplierinfo'].search([('product_tmpl_id','=',self.id)]) ##cek ada ngga data di seller_ids.
#         print "KONDISI CEK SELLER IDS============================",seller ,vals #print ada tidak nya seller_ids
#         if seller:
#             ada_seller = True
#             print "ADA SELLER LHOOO0.1"  
#         if 'seller_ids' in vals: ## jika ada, maka print panjang. selama ada di printkan.     kalo seller[2] ada, maka ada sellernya.
#             print "LEN SELLER_IDS",len(vals['seller_ids']) #print panjang isi dari vals
#             for seller in vals['seller_ids']:
#                 print "========ADA ISI ?=========1.1", seller[2]
#                 if seller[2] != False : #seller[2] untuk mendiskripsikan semua isi dari saller_ids
#                     print "============1"
#                     ada_seller = True #supplier sudah di isi
#                     print "ADA SELLER ?===1.2",ada_seller
#                 else:
#                     ada_seller = False
       
#         elif ada_seller == False : #ketika supplier tidak di isi maka akan keluar warning
#             print "================4"
# #         else: #kalo blm ada suppliernya, masuk kesini :           
#             res_partner_obj = self.env['res.partner'] #cek respartner dengan nama no supplier
#             res_partner = res_partner_obj.search([('name', '=', 'no supplier')])
#         #             print "+++++++++++++++++++++=+++++++++++"
#             prod_supplier = self.env['product.supplierinfo']
#             if res_partner: ##jika ada respartnernya,
#         #                 print "testttttttttttttttt", vals_supplier
#                 prod_supplier.create({
#                                       'product_tmpl_id' : self.id, #self karena sudah ada idnya, jadi panggil diri sendiri.
#                                       'name' : res_partner.id,
#                                       'delay' : 1,
#                                       'min_qty' : 3,
#                                       'price' : 200,
#                                       })
#             else:
#                 vendor = res_partner_obj.create({
#                                                 'name' : "no supplier",
#                                                 'supplier' : True #coba
#                                                 })
#                 prod_supplier.create({
#                                   'product_tmpl_id' : self.id,
#                                   'name' : vendor.id,
#                                    'delay' : 1,
#                                   'min_qty' : 3,
#                                   'price' : 200,       
#                                   }) 
                
#         prod = super(ProductTemplate, self).write(vals)
#         print "===================5"
#         return prod
